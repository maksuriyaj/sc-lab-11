package part5;

import java.util.ArrayList;

public class Refrigerator {
	private int size;
	ArrayList<String> things = new ArrayList<String>();
	
	public Refrigerator(int size){
		this.size = size;
	}
	
	public void put(String stuff) throws FullException{
		if(this.things.size()>=this.size){
			throw new FullException("The refrigerator is full");
		}else{
			this.things.add(stuff);
		}
	}
	public String takeOut(String stuff){
		if(this.things.contains(stuff)){
			return this.things.remove(this.things.indexOf(stuff));
		}
		return null;
	}
	public String toString(){
		String result = "";
		for(String s:this.things){
			result += s + "\n";
		}
		return result;
	}
}
