package test;
import part3.WordCounter;
import part4.DataException;
import part4.FormatException;
import part4.MyClass;
import part5.FullException;
import part5.Refrigerator;


public class Test {

	public static void main(String[] args) {
		partThree();
		partFour();
		partFive();
	}
	public static void partThree(){
		String str = "here is the root of the root and the bud of the bud";
		WordCounter counter = new WordCounter(str);
		counter.count();
		System.out.println("Text: "+str);
		
		System.out.println(counter.hasWord("---------------------------------"));
		System.out.println("Part three: ");
		System.out.println("count root "+counter.hasWord("root"));
		System.out.println("count leaf "+counter.hasWord("leaf"));
		System.out.println("---------------------------------");
	}
	public static void partFour(){
		try{
			MyClass c = new MyClass();
			System.out.print("A");
			c.methX();
			System.out.print("B");
			c.methY();
			return;
		}catch (DataException e){
			System.out.print("D");
		}catch (FormatException e){
			System.out.print("E");
		}finally{
			System.out.print("F");
		}
		System.out.print("G");
	}
	public static void partFive(){
		Refrigerator refrigerator= new Refrigerator(5);
		try{
			refrigerator.put("egg");
			refrigerator.put("candy");
			refrigerator.put("milk");
			refrigerator.put("meat");
			refrigerator.put("juice");
			refrigerator.put("fish");
		}catch(FullException e){
			System.out.println(e.toString());
		}
	}
}
