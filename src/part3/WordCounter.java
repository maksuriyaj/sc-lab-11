package part3;
import java.util.HashMap;

public class WordCounter {
	private String message;
	private HashMap<String, Integer> wordCount = new HashMap<String, Integer>();

	public WordCounter(String message) {
		this.message = message;
	}

	public void count() {
		String[] words = this.message.split(" ");
		for(String word:words){
			if(this.wordCount.containsKey(word)){
				int currentCount = this.wordCount.get(word);
				this.wordCount.replace(word, ++currentCount);
			}else{
				this.wordCount.put(word, 1);
			}
		}
	}

	public int hasWord(String word) {
		for (String key : this.wordCount.keySet()) {
			if (key.equals(word)) {
				return this.wordCount.get(key);
			}
		}
		return 0;
	}
}
